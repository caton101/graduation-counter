#!/bin/python3

# Sources used
#
# PIL stuff: https://pythonprogramming.altervista.org/make-an-image-with-text-with-python/
# PyQt stuff: https://www.geeksforgeeks.org/system-tray-applications-using-pyqt5/

# imports
from subprocess import Popen
from sys import argv
from time import localtime, sleep
from datetime import datetime
from PIL import Image, ImageDraw, ImageFont
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QSystemTrayIcon, QMenu, QAction
import tempfile
from os import remove

# settings
FONT_NAME = "FiraMono-Regular.ttf"  # font name (actual file name)
FONT_SIZE = 30  # font size in point
BACKGROUND_COLOR = (0, 0, 0, 0)  # black, fully transparent
# the color to use before warning is triggered
NORMAL_COLOR = (255, 255, 255, 255)
TIMEOUT = 3600  # one hour
WARNING_THRESHOLD = 14  # number of days until warning color is used
WARNING_COLOR = (255, 255, 100, 255)  # the color when warning is triggered
IMMEDIATE_THRESHOLD = 7  # number of days until immediate color is used
# the color to use when immediate is triggered
IMMEDIATE_COLOR = (255, 100, 100, 255)
GRAD_COLOR = (255, 100, 255, 255)  # the color to use on my graduation day
POST_COLOR = (100, 100, 255, 255)  # the color to use after I graduate

# runtime vars
TRAY_OBJ = None


def getDaysLeft():
    """
    Gets the number of days left until graduation

    Returns:
        int: number of days until graduation
    """
    nowt = localtime()
    nowd = datetime(nowt.tm_year, nowt.tm_mon,
                    nowt.tm_mday, nowt.tm_hour, nowt.tm_min, nowt.tm_sec)
    gradd = datetime(2022, 5, 7, 13, 0, 0)
    delta = gradd - nowd
    return delta.days


def makeImage(days, filename):
    """
    Converts the number of days left into an image

    Args:
        days (int): number of days left until graduation
        filename (str): path for image to be saved
    """
    text = str(days)
    fnt = ImageFont.truetype(FONT_NAME, FONT_SIZE)
    image = Image.new(
        mode="RGBA",
        size=(40, 40),
        color=BACKGROUND_COLOR
    )
    draw = ImageDraw.Draw(image)
    if days < 0:
        color = POST_COLOR
    elif days == 0:
        color = GRAD_COLOR
    elif days <= IMMEDIATE_THRESHOLD:
        color = IMMEDIATE_COLOR
    elif days <= WARNING_THRESHOLD:
        color = WARNING_COLOR
    else:
        color = NORMAL_COLOR
    draw.text((0, 0), text, font=fnt, fill=color)
    image.save(filename)


def refresh():
    """
    Refreshes the tray icon
    """
    if TRAY_OBJ == None:
        print("TRAY_OBJ was none.")
        exit(1)
    days = getDaysLeft()
    filename = tempfile.mktemp(".graduation_counter.png")
    print("Using temp file: " + filename)
    makeImage(days, filename)
    icon = QIcon(filename)
    remove(filename)
    TRAY_OBJ.setIcon(icon)


def runChild():
    """
    Runs the system tray wisget
    """
    global TRAY_OBJ
    app = QApplication([])
    app.setQuitOnLastWindowClosed(False)
    tray = QSystemTrayIcon()
    TRAY_OBJ = tray
    refresh()
    tray.setVisible(True)
    menu = QMenu()
    refreshOpt = QAction("Refresh")
    refreshOpt.triggered.connect(refresh)
    menu.addAction(refreshOpt)
    quitOpt = QAction("Quit")
    quitOpt.triggered.connect(app.quit)
    menu.addAction(quitOpt)
    tray.setContextMenu(menu)
    app.exec_()


def runParent():
    """
    Runs the parent daemon
    """
    while True:
        p = Popen(["python3", argv[0], "--child"])
        try:
            sleep(TIMEOUT)
        except KeyboardInterrupt:
            p.kill()
            exit()
        if p.poll() != None:
            exit()
        else:
            p.kill()


def main():
    """
    Runs either the parent or child daemon
    """
    if len(argv) > 1 and argv[1] == "--child":
        runChild()
    else:
        runParent()


if __name__ == "__main__":
    main()
